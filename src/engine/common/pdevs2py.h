#ifndef __PDEVS2PY_H__
#define __PDEVS2PY_H__

#include <string>

void run_python_module(const std::string&, const std::string&);

#endif
