#!/bin/bash
# executes pdppt QT application forcing a display. Some display must be available on the system
# NOTE: this script is copied during make to <powerdevs>/bin directory

# Script paths
SCRIPT_PATH="${BASH_SOURCE}"
while [ -L "${SCRIPT_PATH}" ]; do
  SCRIPT_DIR="$(cd -P "$(dirname "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"
  SCRIPT_PATH="$(readlink "${SCRIPT_PATH}")"
  [[ ${SCRIPT_PATH} != /* ]] && SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_PATH}"
done
SCRIPT_PATH="$(readlink -f "${SCRIPT_PATH}")"
SCRIPT_DIR="$(cd -P "$(dirname -- "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"


# search for available displays
displays=( $(cd /tmp/.X11-unix && for x in X*; do echo ":${x#X}"; done)   )
# displays=()

qt_display_param=""
if (( ${#displays[@]} )); then
    # there is at least 1 display
	qt_display_param="-display $displays"
else
	echo "Warning: no displays found in tmp/.X11-unix. Attempting to run with GUI"
fi

${SCRIPT_DIR}/pdppt $qt_display_param $@
