

class NetworkLogger(object):
    
    pass


class DefaultLogger(NetworkLogger):
    
    CODE = 0
    
    
class SamplerLogger(NetworkLogger):
    
    CODE = 1
    
    
class DiscreteSamplerLogger(NetworkLogger):
    
    CODE = 2    
