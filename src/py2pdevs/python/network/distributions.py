
class Distribution(object):
    
    def __init__(self):
        pass
    
    def serialize_for(self, argument):
        params = {argument : self.CODE}
        for param, value in self.params().iteritems():
            params['%s_%s' % (argument, param)] = value
        return params
        
    def params(self):
        raise NotImplementedError


class Constant(Distribution):
    
    CODE = 0
    
    VALUE = 'value'
    
    def __init__(self, value):
        self.value = value
        
    def params(self):
        return {
            self.VALUE : self.value
        }
        
class Exponential(Distribution):
    
    CODE = 1
    
    MU = 'mu'
    
    def __init__(self, mu):
        self.mu = mu
        
    def params(self):
        return {
            self.MU : self.mu
        }
        
class Pareto(Distribution):
    
    CODE = 2
    
    SHAPE = 'shape'
    SCALE = 'scale'
    
    def __init__(self, shape, scale):
        self.shape = shape
        self.scale = scale
        
    def params(self):
        return {
            self.SHAPE : self.shape,
            self.SCALE : self.scale
        }
        
class Split(Distribution):
    
    CODE = 3
    
    VAL1 = 'val1'
    VAL2 = 'val2'
    MEAN = 'mean'
    
    def __init__(self, val1, val2, mean):
        self.val1 = val1
        self.val2 = val2
        self.mean = mean
        
    def params(self):
        return {
            self.VAL1 : self.val1,
            self.VAL2 : self.val2,
            self.MEAN : self.mean
        }
        
class Normal(Distribution):
    
    CODE = 4
    
    MU = 'mu'
    VAR = 'var'
    
    def __init__(self, mu=None, var=None):
        self.mu = mu or 0
        self.var = var or 1
        
    def params(self):
        return {
            self.MU : self.mu,
            self.VAR : self.var
        }
        
class Poisson(Distribution):    
    CODE = 6    
    RATE = 'rate'
    
    def __init__(self, rate):
        self.rate = rate        
        
    def params(self):
        return {
            self.RATE : self.rate,            
        }  
        
class Gamma(Distribution):    
    CODE = 8    
    SHAPE = 'shape'
    SCALE = 'scale'
    
    def __init__(self, shape, scale):
        self.shape = shape
        self.scale = scale                
        
    def params(self):
        return {
            self.SHAPE : self.shape,            
            self.SCALE : self.scale,
        }                