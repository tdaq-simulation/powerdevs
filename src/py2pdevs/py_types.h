#ifndef _PY2PDEVS_TYPES_H_
#define _PY2PDEVS_TYPES_H_

#include <boost/python.hpp>

typedef boost::python::str py_str;
typedef boost::python::list py_list;
typedef boost::python::dict py_dict;

#endif
