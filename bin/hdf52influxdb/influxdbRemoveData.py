import os
import sys
import argparse
from influxdbHandler import InfluxdbHandler


def checks(config_file, database_or_bucket_name):

    if config_file == None:
        print('Set a valid config file.')
        sys.exit()

    if database_or_bucket_name == None:
        print('Set a valid database/bucket name.')
        sys.exit()

def main(config_file, database_or_bucket_name):

    checks(config_file, database_or_bucket_name)

    writer = InfluxdbHandler(None, config_file, database_or_bucket_name, None, None)
    writer.delete_database(database_or_bucket_name)
    print(f'{database_or_bucket_name} removed')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Remove data from an InfluxDB instance.')
        
    parser.add_argument(
        '--config_file',
        default=os.path.join(os.path.dirname(__file__), 'config.ini'), type=str,
        help='The name or the full path of the config file.'
    )
    
    parser.add_argument(
        '--database_or_bucket_name',
        default=None, type=str,
        help='The name of the bucket or database to remove.'
    )

    args = parser.parse_args()
    config_file = args.config_file
    database_or_bucket_name = args.database_or_bucket_name
    main(config_file, database_or_bucket_name)
