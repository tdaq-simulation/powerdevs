import os
import sys
from sys import getsizeof
import time
import math
import h5py
import argparse
import multiprocessing
from influxdbHandler import InfluxdbHandler


def checks(h5_file, config_file):

    if h5_file == None:
        print('Set a valid h5 file.')
        sys.exit()

    if config_file == None:
        print('Set a valid config file.')
        sys.exit()

def initGlobals(time_precision, time_scalar):
    
    global queue_
    queue_ = multiprocessing.Manager().Queue()
    
    global time_precision_options_, time_precision_, time_scalar_
    time_precision_options_ = {'ns': 10**9, 'us': 10**6, 'ms': 10**3, 's': 1}
    time_precision_ = time_precision
    time_scalar_ = time_scalar
    
    global stopTimeUNIX_, flagLosingPrecision_, countLosingPrecision_
    stopTimeUNIX_ = 0
    flagLosingPrecision_ = False
    countLosingPrecision_ = 0
    
    global timeCreatePrefix_, timeLogDataset_, countDatasets_, countMeasurements_, countProtocolsBytes_
    timeCreatePrefix_ = []
    timeLogDataset_ = []
    countDatasets_ = 0
    countMeasurements_ = 0
    countProtocolsBytes_ = 0

def getTimeUNIX(time: float):

    global time_precision_options_, time_precision_, time_scalar_
    global stopTimeUNIX_, flagLosingPrecision_, countLosingPrecision_

    time_rounded = round(time, int(math.log10(time_scalar_) + math.log10(time_precision_options_[time_precision_])))
    time_unix = int(time_rounded * time_scalar_ * time_precision_options_[time_precision_])

    if time != time_rounded:
        
        if not(flagLosingPrecision_):
            
            print('')
            print('Warning! Losing precision.')
            print('Converting timestamps of type double (saved by the simulation) to UNIX has an effect on the precision.')
            print('Precision can also be affected by the method used by python to represent doubles.')
            print('')
            print('In general, Influx default precision is ns.')
            print('In influx, measurements with same names, tags and timestamps are overwrited.')
            print('Grafana plots have ms precision.')
            print('If more precision is needed use time_scalar.')
            print("For example: time_precision = 'ns' & time_scalar = 10**6 => In Grafana 1 ms is 1 ns in reallity.")
            print("To set the range of plots in grafana, append the following in the url '&from=0&to=unix_in_ms'")
            
            flagLosingPrecision_ = True
        countLosingPrecision_ += 1
        # print(f"time={time}, {time:.20f} ; time_rounded={time_rounded:.20f} ; time_unix={time_unix:.20f}")
        
    if time_unix > stopTimeUNIX_:
        stopTimeUNIX_ = time_unix
    
    return time_unix

def logDataset(key: str, dataset):

    global queue_
    global timeCreatePrefix_, timeLogDataset_, countDatasets_, countMeasurements_, countProtocolsBytes_

    if isinstance(dataset, h5py.Dataset) and not(key.startswith('Parameters/'))and dataset[:].size != 0:

        dataset = dataset[:]

        t0 = time.perf_counter()
        tokens = key.split("/")
        lineProtocolPrefix = f'{tokens[-1]},'  # create measurement name
        for idx, tag_value in enumerate(tokens[:-1]):  # append tags
            lineProtocolPrefix += f'level{idx}={tag_value}'
            lineProtocolPrefix += ' ' if idx == len(tokens[:-1]) - 1 else ','
        timeCreatePrefix_.append([time.perf_counter(), time.perf_counter() - t0])

        measurements = []
        count_dataset_flag = False
        for col in range(dataset.shape[1]):
            if not(math.isnan(dataset[1][col])):  # if value not NaN
                lineProtocol = lineProtocolPrefix + f'value={dataset[1][col]} {getTimeUNIX(dataset[0][col])}'  # append value and time
                measurements.append(lineProtocol)
                countProtocolsBytes_ += getsizeof(lineProtocol)
                count_dataset_flag = True
                if len(measurements) >= 10000:
                    queue_.put(measurements)
                    countMeasurements_ += len(measurements)
                    measurements = []

        if len(measurements) != 0:
            queue_.put(measurements)
            countMeasurements_ += len(measurements)

        if count_dataset_flag:
            countDatasets_ += 1

        timeLogDataset_.append([time.perf_counter(), time.perf_counter() - t0])

    return

def performanceManager(h5_file, writer, t0, t1):
    # libraries required only for the performance tracking and plotting
    import matplotlib.pyplot as plt
    import pandas as pd
    
    # Create dataframes
    df_timeQueueGet = pd.DataFrame(list(writer.timeQueueGet), columns=['time', 'value'])
    df_timeQueueGet.time = df_timeQueueGet.time - t0
    df_timeQueueGet = df_timeQueueGet.set_index('time')
    df_timeWrite = pd.DataFrame(list(writer.timeWrite), columns=['time', 'value'])
    df_timeWrite.time = df_timeWrite.time - t0
    df_timeWrite = df_timeWrite.set_index('time')
    df_timeLogDataset = pd.DataFrame(list(timeLogDataset_), columns=['time', 'value'])
    df_timeLogDataset.time = df_timeLogDataset.time - t0
    df_timeLogDataset = df_timeLogDataset.set_index('time')

    # Print performance info
    print('')
    print(f'Total Time: {t1 - t0} seconds')
    print(f'QueueGet Total Time: {df_timeQueueGet.sum()[0]} seconds')
    print(f'Write Total Time: {df_timeWrite.sum()[0]} seconds')
    print(f'LogDataset Total Time: {df_timeLogDataset.sum()[0]} seconds')
    print(f'Logged Datasets: {countDatasets_}')
    print(f'Datasets Avg Size: {countMeasurements_/countDatasets_} measurements')
    print(f'Logged Measurements: {countMeasurements_}')
    print(f'Total Size Of Protocols: {countProtocolsBytes_} bytes')
    print(f'Simulation Stop Time in UNIX: {stopTimeUNIX_} {time_precision_}')
    print('')

    # Save dataframes    
    dfs_path = os.path.join(os.path.basename(__file__), 'saves', h5_file.split('/')[-1][:-3])
    if not os.path.exists(dfs_path):
        os.makedirs(dfs_path)

    df_timeQueueGet.to_csv(os.path.join(dfs_path, 'df_timeQueueGet.csv'), index=True)
    df_timeWrite.to_csv(os.path.join(dfs_path, 'df_timeWrite.csv'), index=True)
    df_timeLogDataset.to_csv(os.path.join(dfs_path, 'df_timeLogDataset.csv'), index=True)

    # Plot performance info
    fig, axs = plt.subplots(2, 2)
    axs[0, 0].hist(df_timeQueueGet.value, bins=100)
    axs[0, 0].set_title('timeQueueGet (s)')
    axs[0, 0].text(0.5, 0.5, f'count: {df_timeQueueGet.count()[0]}\nsum: {df_timeQueueGet.sum()[0]}\nmax: {df_timeQueueGet.max()[0]}\nmean: {df_timeQueueGet.mean()[0]}\nmin: {df_timeQueueGet.min()[0]}', transform=axs[0, 0].transAxes)
    axs[0, 1].hist(df_timeWrite.value, bins=100)
    axs[0, 1].set_title('timeWrite (s)')
    axs[0, 1].text(0.5, 0.5, f'count: {df_timeWrite.count()[0]}\nsum: {df_timeWrite.sum()[0]}\nmax: {df_timeWrite.max()[0]}\nmean: {df_timeWrite.mean()[0]}\nmin: {df_timeWrite.min()[0]}', transform=axs[0, 1].transAxes)
    axs[1, 0].hist(df_timeLogDataset.value, bins=100)
    axs[1, 0].set_title('timeLogDataset (s)')
    axs[1, 0].text(0.5, 0.5, f'count: {df_timeLogDataset.count()[0]}\nsum: {df_timeLogDataset.sum()[0]}\nmax: {df_timeLogDataset.max()[0]}\nmean: {df_timeLogDataset.mean()[0]}\nmin: {df_timeLogDataset.min()[0]}', transform=axs[1, 0].transAxes)
    axs[1, 1].plot(df_timeQueueGet, 'o--', markersize=3, label='timeQueueGet')
    axs[1, 1].plot(df_timeWrite, 'o--', markersize=3, label='timeWrite')
    axs[1, 1].plot(df_timeLogDataset, 'o--', markersize=3, label='timeLogDataset')
    axs[1, 1].legend()
    
    plt.savefig(os.path.join(dfs_path, 'histograms.png'), bbox_inches='tight')
    plt.show() 

def main(h5_file, config_file, time_precision, time_scalar, name, track_performance):

    # Make checks
    checks(h5_file, config_file)
    
    # Set global variables
    initGlobals(time_precision, time_scalar)

    # Load h5 file
    h5file = h5py.File(h5_file, 'r')

    # Start InfluxDB writer
    writer = InfluxdbHandler(h5_file, config_file, name, time_precision_, queue_)
    writer.start()

    # Start
    t0 = time.perf_counter()    
    h5file.visititems(logDataset)
    queue_.put(None)
    queue_.join()
    t1 = time.perf_counter()

    # Manage perfomance info
    if track_performance:
        performanceManager(h5_file, writer, t0, t1)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Transfer data from a HDF5 file to an InfluxDB instance.')
    
    parser.add_argument(
        '--h5_file',
        default=None, type=str,
        help='The name or the full path of the HDF5 file.'
    )
    
    parser.add_argument(
        '--config_file',
        default=os.path.join(os.path.dirname(__file__), 'config.ini'), type=str,
        help='The name or the full path of the config file.'
    )
    
    parser.add_argument(
        '--time_precision',
        default='ns', type=str,
        help='The time precision, that InfluxDB will save timestamps. Can be [ns, us, ms, s].'
    )
    
    parser.add_argument(
        '--time_scalar',
        default=1, type=int,
        help='Used to trick influx and grafana, in order to achieve better precision. Must be powers of 10.'
    )
    
    parser.add_argument(
        '--track_performance',
        default=False, type=bool,
        help='Track, save and plot performance metrics of uploading results to influxDB.'
    )  
   
    parser.add_argument(
        '--name',
        default='', type=str,
        help='The name of database/bucket.'
    )    

    args = parser.parse_args()
    h5_file = args.h5_file
    config_file = args.config_file
    time_precision = args.time_precision
    time_scalar = args.time_scalar
    track_performance = args.track_performance    
    name = args.name    
    main(h5_file, config_file, time_precision, time_scalar, name, track_performance)
