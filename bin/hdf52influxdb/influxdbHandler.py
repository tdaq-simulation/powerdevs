import os
import sys
import json
import time
import configparser
import multiprocessing
from influxdb_client import InfluxDBClient, WriteOptions, BucketRetentionRules
from influxdb_client.client.write_api import WriteType


def success_cb(details, data):
    # url, token, org = details
    # print(url, token, org)
    # data = data.decode('utf-8').split('\n')
    # print('Total Rows Inserted:', len(data))  
    return

def error_cb(details, data, exception):
    print(exception)

def retry_cb(details, data, exception):
    print('Retrying because of an exception:', exception) 


class InfluxdbHandler(multiprocessing.Process):

    def __init__(self, h5_file, config_file, name, time_precision, queue):
        
        multiprocessing.Process.__init__(self)

        # Read config file
        config = configparser.ConfigParser()
        config.read(config_file)
        
        self.time_precision = time_precision
        self.queue = queue
        self.timeQueueGet = multiprocessing.Manager().list()
        self.timeWrite = multiprocessing.Manager().list()

        self.dbversion = config.get('influxdbAuthentication', 'version')
        self.host = config.get('influxdbAuthentication', 'host')
        self.port = config.get('influxdbAuthentication', 'port')
        self.username = config.get('influxdbAuthentication', 'username')
        self.password = config.get('influxdbAuthentication', 'password')        
    
        if self.dbversion == '1.8.3':
            self.client = InfluxDBClient(url=f'{self.host}:{self.port}', token=f'{self.username}:{self.password}', org='-', debug=False)
            self.influx_cli_cmnd = f"influx --username {self.username} --password {self.password} --ssl -host {self.host[8:]} -port {self.port}"
            if h5_file != None:
                if name != '':
                    self.database = name
                else:
                    self.database = h5_file.split('/')[-1][:-3]
                self._checkDatabaseExistence()
                self.retention_policy = self._findRetentionPolicy()

        if self.dbversion == '2.4.0':
            self.org = config.get('influxdbAuthentication', 'org')
            self.token = config.get('influxdbAuthentication', 'token')
            self.client = InfluxDBClient(url=f'{self.host}:{self.port}', token=self.token, org=self.org, debug=False)
            self.buckets_api = self.client.buckets_api()
            if h5_file != None:
                if name != '':
                    self.bucket = name
                else:
                    self.bucket = h5_file.split('/')[-1][:-3]
                self._checkBucketExistence()

        self.write_api = self.client.write_api(success_callback=success_cb, error_callback=error_cb, retry_callback=retry_cb,
            write_options=WriteOptions(write_type=WriteType.batching, batch_size=50_000, flush_interval=10_000))
        
    def _checkDatabaseExistence(self):
        
        # For influxdb version 1.8.3
        
        # Check if the database exist in influx instance
        dbs = os.popen(f"{self.influx_cli_cmnd} -format=json -execute 'SHOW DATABASES'").read()
        dbs = json.loads(dbs)
        dbs = dbs['results'][0]['series'][0]['values']

        if [self.database] in dbs:
            print('')
            print('Database already exist and may contain data.')
            overwrite = input('Overwrite? (y/n): ')
            if overwrite == 'y':
                # Delete database
                os.system(f"{self.influx_cli_cmnd} -execute 'DROP DATABASE '{self.database}''")
            else:
                sys.exit()

        # Create database with default retention policy (autogen)
        os.system(f"{self.influx_cli_cmnd} -execute 'CREATE DATABASE '{self.database}''")

    def _findRetentionPolicy(self):

        # For influxdb version 1.8.3
        
        # Find the retention policy being used, it will be used for creating the bucket name
        retention_policy = os.popen(f"{self.influx_cli_cmnd} -format=json -execute 'SHOW RETENTION POLICIES ON {self.database}'").read()
        retention_policy = json.loads(retention_policy)
        retention_policy = retention_policy['results'][0]['series'][0]['values'][0][0]

        return retention_policy

    def _checkBucketExistence(self):

        # For influxdb version 2.4.0
        
        buckets = [bucket.name for bucket in self.buckets_api.find_buckets().buckets]

        if self.bucket in buckets:
            print('')
            print('Database already exist and may contain data.')
            overwrite = input('Overwrite? (y/n): ')
            if overwrite == 'y':
                bucket = self.buckets_api.find_bucket_by_name(self.bucket)
                self.buckets_api.delete_bucket(bucket)
            else:
                sys.exit()

        retention_rules = BucketRetentionRules(type="expire", every_seconds=0)
        self.buckets_api.create_bucket(bucket_name=self.bucket, retention_rules=retention_rules, org=self.org)

    def run(self):

        while True:
            t0 = time.perf_counter()
            next_task = self.queue.get()
            self.timeQueueGet.append([time.perf_counter(), time.perf_counter() - t0])
            
            if next_task is None:
                # Poison pill means terminate
                self.terminate()
                self.queue.task_done()
                break

            t1 = time.perf_counter()
            
            if self.dbversion == '1.8.3':
                self.write_api.write(bucket=f'{self.database}/{self.retention_policy}', record=next_task, write_precision=self.time_precision)  # influx 1.8.3
            
            if self.dbversion == '2.4.0':
                self.write_api.write(bucket=self.bucket, record=next_task, write_precision=self.time_precision)  # influx 2.4.0
            
            self.timeWrite.append([time.perf_counter(), time.perf_counter() - t1])
            self.queue.task_done()

    def delete_database(self, database_or_bucket_name):
        if self.dbversion == '1.8.3':
            os.system(f"{self.influx_cli_cmnd} -execute 'DROP DATABASE '{database_or_bucket_name}''")
        if self.dbversion == '2.4.0':
            bucket = self.buckets_api.find_bucket_by_name(database_or_bucket_name)
            self.buckets_api.delete_bucket(bucket)

    def terminate(self) -> None:
        proc_name = self.name
        print()
        print('Writer: flushing data...')
        self.write_api.close()
        self.client.close()
        print('Writer: closed'.format(proc_name))