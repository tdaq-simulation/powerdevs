# **Readme for Devs**

  This file contains notes about possible updates of individual parts of this tool.


# **Table of Contents**

- [**Readme for Devs**](#readme-for-devs)
- [**Table of Contents**](#table-of-contents)
- [**1. Grafana**](#1-grafana)
- [**2. InfluxDB**](#2-influxdb)
- [**3. Docker**](#3-docker)
- [**4. Scripts**](#4-scripts)

 See the link below for more details:

  - <https://docs.influxdata.com/influxdb/v1.8/flux/flux-vs-influxql/>

# **1. Grafana**

  Grafana is a multi-platform open source web application, that provides tools to visualise and analyse data. It can connect to different data sources and the end users can create complex monitoring dashboards using interactive query builders. A plug-in system is used to expand its possibilities.

  This tool provides **grafana enterprise 8.4.3** as both local and remote instance. This choice was made as CERN IT provides this version and also this version seems to work better compared to latest versions.

  Some drawbacks noticed in this version are:

  - Time series panel supports only milliseconds precision. (Points with nanoseconds precision will be mapped to the closest millisecond. As a result, you can see different points with the same timestamp.)
  - Histogram panel does not provide variety of options.

  However, up to the enterprise version 9.1.1 there does not seem to be any improvment. To find more information about grafana and the available version, visit the links bellow:

  - <https://grafana.docs.cern.ch/>
  - <https://grafana.com/grafana/>
  - <https://grafana.com/grafana/download>


## **1.1 Configure datasources **

Instructions on how to configure InfluxDB datasources can be found:
  - <https://dbod-user-guide.web.cern.ch/getting_started/influxdb/#configuring-an-influxdb-data-source-with-flux-support-in-grafana/>
  NOTE: in the 'token' field (a password) leave it empty


# **2. InfluxDB**

  InfluxDB is an open-source time series database (TSDB) developed by the company InfluxData. It is used for storage and retrieval of time series data in fields such as operations monitoring, application metrics, Internet of Things sensor data, and real-time analytics.

  This tool provides **InfluxDB v1.8.3** as a remote service but also **InfluxDB v2.4.0**, which is the latest one at the time this tool is developed, as local. InfluxDB v1.8.3 was chosen because is provided by CERN Batabase-on-Demand (DBOD) service.

  In versions 1.x, inside an influxdb instance you can have multiple databases and a database can have multiple retentions. On the other hand, in versions 2.x, inside an influxdb instance you can have multiple buckets and a bucket has only one retention policy. Officially versions 1.x support only InfluxQL but versions 2.x both InfluxQL and Flux. However, dealing with both version at the same time is more confusing. So, when working with them, have in mind the notes below:

  -  Influx CLI v1.10.8 can be used to access influxdb version 1.x. For versions 2.x another version is needed.
  - Versions 1.x use username and password to authenticate but versions 2.x use username, password, organization and token.
  -  If you want to use InfluxQL for version 2.x in Grafana, first you have to map buckets to a database and a retention policy (<https://docs.influxdata.com/influxdb/v2.3/query-data/influxql/#map-unmapped-buckets>).
  -  InfluxDB python-client (<https://github.com/influxdata/influxdb-client-python>) support InfluxDB versions 1.8.x and 2.x.
  -  Connect to a 1.8 instance using the the following example (<https://github.com/influxdata/influxdb-client-python/blob/master/examples/influxdb_18_example.py>).
  -  However, InfluxDB python-clienthis client does not fully supports 1.8 versions (<https://github.com/influxdata/influxdb-client-python#influxdb-1-8-api-compatibility>). It is important to note that python-client provides a bucket-api to manage buckets, although this is not supported for 1.8 versions. To manage databases using version 1.8, use influx CLI v1.10.8 (see influxdbHandler.py).

  More documentation about these versions, can be found in the links below:

  - <https://dbod-user-guide.web.cern.ch/getting_started/influxdb/>
  - <https://docs.influxdata.com/influxdb/v1.8/>
  - <https://docs.influxdata.com/influxdb/v2.4/>


# **3. Docker**

  In this tool, docker-compose plugin is used. You can modify the versions and the authentication credentials by using the **powerdevs/docker/grafana/docker-compose.yml** file.

  Changing this file, keep in mind that:

  - If you remove a container, the corresponing volumes are not removed. So, if you have for example a container with InfluxDB instance that has data. If you remove the container data still exist inside the volume. If you do not remove the volume manually, if you start again a container pointing in the same volume, InfluxDB will have the same data again.
  - Grafana uses two more volumes:
    - *./grafana/provisioning/datasources* that is used for configuring datasources.
    - *./grafana/provisioning/dashboards* that is used for configuring dashboards.

  Useful links abour InfluxDB and Grafana images configuration:

  - <https://hub.docker.com/_/influxdb>
  - <https://www.influxdata.com/blog/running-influxdb-2-0-and-telegraf-using-docker/>
  - <https://grafana.com/docs/grafana/latest/setup-grafana/installation/docker/>
  - <https://grafana.com/docs/grafana/latest/administration/provisioning/>

  Useful git repositories:

  - <https://github.com/tushardhadiwal/docker-influxdb-grafana>
  - <https://github.com/samuelebistoletti/docker-statsd-influxdb-grafana>
  - <https://github.com/guessi/docker-compose-grafana-influxdb>


# **4. Scripts**

The main task of this tool, sending data to an Influx instance, is done by the following two scripts:

- **influxdbHandler.py**: A class wrapping the influxdb python-client.
- **hdf52influxdb.py**: Reading an HDF5 and send data to influx.
  
See this example:

- <https://github.com/influxdata/influxdb-client-python/blob/master/examples/import_data_set_multiprocessing.py>

