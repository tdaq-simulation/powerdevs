#include "TcpSndCoupled.h"

void TcpSndCoupled::createModel(){
	// helper variables so that copy&paste works right away
	Simulator* D0[] = {this};
	Coupling* mainCoupling = nullptr;
	double ti =0;


	// copy paste the code generated by powerdevs in build.h here (generate the code with only the coupled model you want)

	//D0[0] is TCP_session1
//	      D0[0] = new Coupling("TCP_session1");
	      ((Simulator*)D0[0])->father = mainCoupling;
	       Simulator **D1 = new Simulator* [6];
	       Connection **EIC1 = new Connection* [1];
	       Connection **EOC1 = new Connection* [1];
	       Connection **IC1 = new Connection* [7];
	        //D1[0] is  FlowGenerator
	        D1[0] = new PacketFlowGenerator("FlowGenerator");
	        ((Simulator*)D1[0])->father = (Coupling*)D0[0];
	        D1[0]->init(0.0,"TCP_FLOWID_1");
	        //D1[1] is  CustomSteps1
	        D1[1] = new CustomSteps("CustomSteps1");
	        ((Simulator*)D1[1])->father = (Coupling*)D0[0];
	        D1[1]->init(0.0,"DiscreteTcpSender1.TCP_session1.packetGenTime","DiscreteTcpSender1.TCP_session1.packetGenValue");
	        //D1[2] is  TCP_SND
	        D1[2] = new packettcpsnd("TCP_SND");
	        ((Simulator*)D1[2])->father = (Coupling*)D0[0];
	        D1[2]->init(0.0,"DiscreteTcpSender1.TCP_session1.TCP_SND.MSS","DiscreteTcpSender1.TCP_session1.TCP_SND.CWND_MAX","DiscreteTcpSender1.TCP_session1.TCP_SND.CWND_SSHTHRESH","DiscreteTcpSender1.TCP_session1.TCP_SND.RTT_ALPHA","DiscreteTcpSender1.TCP_session1.TCP_SND.RTT_INITIAL","DiscreteTcpSender1.TCP_session1.TCP_SND.DUP_ACK_LIMIT","DiscreteTcpSender1.TCP_session1.TCP_SND.T_FORCED_RTO","DiscreteTcpSender1.TCP_session1.TCP_SND.INTERPACKET_SND_TIME","DiscreteTcpSender1.TCP_session1.TCP_SND.INTER_REQ_TIME");
	        //D1[3] is  AppQueue
	        D1[3] = new packetqueue("AppQueue");
	        ((Simulator*)D1[3])->father = (Coupling*)D0[0];
	        D1[3]->init(0.0,"-1","-1");
	        //D1[4] is  sentPacket
	        D1[4] = new packetLogger("sentPacket");
	        ((Simulator*)D1[4])->father = (Coupling*)D0[0];
	        D1[4]->init(0.0);
	        //D1[5] is  packetFlow
	        D1[5] = new packetLogger("packetFlow");
	        ((Simulator*)D1[5])->father = (Coupling*)D0[0];
	        D1[5]->init(0.0);
	       EIC1[0] = new Connection();
	       EIC1[0]->setup(0,0,2,1);
	       EOC1[0] = new Connection();
	       EOC1[0]->setup(2,0,0,0);
	       IC1[0] = new Connection();
	       IC1[0]->setup(2,1,3,1);
	       IC1[1] = new Connection();
	       IC1[1]->setup(3,0,2,0);
	       IC1[2] = new Connection();
	       IC1[2]->setup(1,0,0,0);
	       IC1[3] = new Connection();
	       IC1[3]->setup(2,1,0,0);
	       IC1[4] = new Connection();
	       IC1[4]->setup(2,0,4,0);
	       IC1[5] = new Connection();
	       IC1[5]->setup(0,0,5,0);
	       IC1[6] = new Connection();
	       IC1[6]->setup(0,0,3,0);
	      ((Coupling*)D0[0])->setup(D1,6,IC1,7,EIC1,1,EOC1,1);
	      ((Coupling*)D0[0])->init(ti);
}
