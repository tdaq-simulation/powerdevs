//CPP:network/packettcprcv.cpp
#if !defined packettcprcv_h
#define packettcprcv_h

#include "math.h"

#include "simulator.h"
#include "event.h"
#include "stdarg.h"

//#include "packettool.h" // TODO: NetworkPacket is a better implementation but needs to be use in all models (one day)
#include "NetworkData/NetworkPacket.h"
#include "NetworkData/Protocol/TcpIpProtocol.h"
#include "hybrid/stdevstool.h"
#include "sinks/Loggers/IPowerDEVSLogger.h"
#include "sinks/Loggers/ConfigurationLogger.h" // TODO: delete this include afterwards, when using baseSimulator as base class
#include "general/BaseSimulator.h"
#include "NetworkData/Protocol/RoutingProtocol.h"

class packettcprcv: public BaseSimulator {

	// Parameters
	std::string ipSender;
	int portSender;
	std::string ipReceiver;
	int portReceiver;
	int MSS;

	// State variables
	std::shared_ptr<NetworkPacket> arrivedPacket;

	int DATA_SEQ_NUMBER;
	int SEQ_NEXT_Expected;
	int SEQ_LAST_Valid;
	int SEQ_DUP;
	int SEQ_NXT;
	int ReceivedACKedFromPeerLayer;
	int SendACKtoPeerLayer;
	double DATA_TCP_SND_TIMESTAMP;

public:
	packettcprcv(const char *n): BaseSimulator(n) {};
	void init(double, ...);
	//	double ta(double t);
	void dint(double);
	void dext(Event , double );
	Event lambda(double);
	//	void exit(double t);
private:
	bool validatePacket(std::shared_ptr<NetworkPacket>& packet);
};
#endif
